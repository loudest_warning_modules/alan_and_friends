## Alan and Friends
**_A Loudest Warning adaptation of Music Thing Modular Turing Machine, Volts and Pulses expanders._**

No need to introduce again the wonderful Turing Machine, which is a semi-random looping sequencer.
In this version, instead of only having the main sequencer, this module has all "sequencing" expanders included.
That makes a kind of "meta random sequencer", for the modest size of 4 horizontal units.

You have a regular "Turing machine" on the left, one Volts in the middle and Pulses outputs on the right.

This is a dense project, but each individual block is not hard to understand. All the circuitry is available on the back, even with the module all set. It consists of two PCBs stacked on each other.

Expanders connectors (gates and pulses) are compatible with the original expanders : that means you can adapt existing Eurorack PCBs or make your own!

The "calibration" procedure mostly stays the same as the Eurorack version.

![alt text](front.JPG "built module")


### How does it work?
![alt text](interface.png "explainations")

### Repository contents:
- The hardware files:
    - The whole KiCad project
    - The Bill of Materials (BoM)
    - The gerber files used for the batch
    - A pdf export of the schematics
- Panel designs:
    - The SVG for the panel design
    - The exported pdf
- Some miscellaneous documentation:
    - Original schematics

See the **build_doc** folder for build notes and circuit tweaks.

After several months of development and 3 prototypes, the project is finally available in DIY-only (since LW is mainly about DIY). A R*S panel design might eventually follow.

For more information see https://musicthing.co.uk/pages/turing.html

... And the [Muffwiggler](https://www.muffwiggler.com/forum/viewtopic.php?p=3382185) thread.

## Technical specs

4U module for the [Loudest Warning](https://loudestwarning.tumblr.com/4Umodular) format.

Outputs:
- Pulses: 0-5V
- Noise: +/-5V (typ.)
- CV1: 0-10V
- CV2: 0-10V

Inputs:
- Sequence modulation: +/-5V

Depth:

Current draw: **?mA** on -12V, **?mA** on +12V.
