EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "Alan and Friends"
Date ""
Rev "2"
Comp "phonemes"
Comment1 "Based on Turing Machine by Music Thing Modular"
Comment2 "CC-BY-SA"
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 10000 1150 0    47   Input ~ 0
BIT1
$Comp
L 4xxx:4081 U?
U 1 1 5DE39812
P 6250 2850
AR Path="/5DE395EF/5DE39812" Ref="U?"  Part="1" 
AR Path="/5DE3964E/5DF7D519/5DE39812" Ref="U10"  Part="1" 
F 0 "U10" H 6250 3175 50  0000 C CNN
F 1 "4081" H 6250 3084 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6250 2850 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 6250 2850 50  0001 C CNN
	1    6250 2850
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U?
U 2 1 5DE398FD
P 6250 1650
AR Path="/5DE395EF/5DE398FD" Ref="U?"  Part="2" 
AR Path="/5DE3964E/5DF7D519/5DE398FD" Ref="U10"  Part="2" 
F 0 "U10" H 6250 1333 50  0000 C CNN
F 1 "4081" H 6250 1424 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6250 1650 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 6250 1650 50  0001 C CNN
	2    6250 1650
	1    0    0    1   
$EndComp
$Comp
L 4xxx:4081 U?
U 3 1 5DE3993A
P 6250 1000
AR Path="/5DE395EF/5DE3993A" Ref="U?"  Part="3" 
AR Path="/5DE3964E/5DF7D519/5DE3993A" Ref="U10"  Part="3" 
F 0 "U10" H 6250 1325 50  0000 C CNN
F 1 "4081" H 6250 1234 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6250 1000 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 6250 1000 50  0001 C CNN
	3    6250 1000
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U?
U 4 1 5DE3998C
P 6250 2250
AR Path="/5DE395EF/5DE3998C" Ref="U?"  Part="4" 
AR Path="/5DE3964E/5DF7D519/5DE3998C" Ref="U10"  Part="4" 
F 0 "U10" H 6250 2575 50  0000 C CNN
F 1 "4081" H 6250 2484 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 6250 2250 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 6250 2250 50  0001 C CNN
	4    6250 2250
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U?
U 5 1 5DE39A09
P 7700 1900
AR Path="/5DE395EF/5DE39A09" Ref="U?"  Part="5" 
AR Path="/5DE3964E/5DF7D519/5DE39A09" Ref="U10"  Part="5" 
F 0 "U10" H 7930 1946 50  0000 L CNN
F 1 "4081" H 7930 1855 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 7700 1900 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 7700 1900 50  0001 C CNN
	5    7700 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 900  5650 900 
Wire Wire Line
	5950 2150 5650 2150
Wire Wire Line
	5950 2350 5650 2350
Wire Wire Line
	5950 2750 5250 2750
Wire Wire Line
	5950 2950 5150 2950
$Comp
L power:+12VA #PWR?
U 1 1 5DE76E2F
P 8050 1350
AR Path="/5DE395EF/5DE76E2F" Ref="#PWR?"  Part="1" 
AR Path="/5DE3964E/5DF7D519/5DE76E2F" Ref="#PWR0167"  Part="1" 
F 0 "#PWR0167" H 8050 1200 50  0001 C CNN
F 1 "+12VA" H 8065 1523 50  0000 C CNN
F 2 "" H 8050 1350 50  0001 C CNN
F 3 "" H 8050 1350 50  0001 C CNN
	1    8050 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 1350 7700 1350
$Comp
L Device:C C?
U 1 1 5DE83D2A
P 8350 1950
AR Path="/5DE395EF/5DE83D2A" Ref="C?"  Part="1" 
AR Path="/5DE3964E/5DF7D519/5DE83D2A" Ref="C27"  Part="1" 
F 0 "C27" H 8465 1996 50  0000 L CNN
F 1 "100n" H 8465 1905 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D8.0mm_W2.5mm_P5.00mm" H 8388 1800 50  0001 C CNN
F 3 "~" H 8350 1950 50  0001 C CNN
	1    8350 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 1350 8350 1800
Wire Wire Line
	7700 1350 7700 1400
Text HLabel 6900 1000 2    47   Output ~ 0
PULSE_12
Text HLabel 10000 1350 0    47   Input ~ 0
BIT2
Text HLabel 10000 2350 0    47   Input ~ 0
BIT7
Text HLabel 10000 1750 0    47   Input ~ 0
BIT4
Wire Wire Line
	6550 1000 6600 1000
Text HLabel 6900 2250 2    47   Output ~ 0
PULSE_47
Wire Wire Line
	6550 2250 6600 2250
Text HLabel 6900 2850 2    47   Output ~ 0
PULSE_24
Wire Wire Line
	6550 2850 6900 2850
Text HLabel 10000 1550 0    50   Input ~ 0
BIT3
Text HLabel 10000 1950 0    50   Input ~ 0
BIT5
Text HLabel 10000 2150 0    50   Input ~ 0
BIT6
Text Notes 550  7700 0    50   ~ 0
\n
Text HLabel 10000 2550 0    50   Input ~ 0
BIT8
Text HLabel 4700 7100 0    50   Input ~ 0
CLOCK
$Comp
L 4xxx:4081 U13
U 1 1 5E38F87E
P 1650 1050
F 0 "U13" H 1650 1375 50  0000 C CNN
F 1 "4081" H 1650 1284 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 1650 1050 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 1650 1050 50  0001 C CNN
	1    1650 1050
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U13
U 2 1 5E38F999
P 1650 1600
F 0 "U13" H 1650 1925 50  0000 C CNN
F 1 "4081" H 1650 1834 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 1650 1600 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 1650 1600 50  0001 C CNN
	2    1650 1600
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U13
U 3 1 5E38F9F1
P 1650 2150
F 0 "U13" H 1650 2475 50  0000 C CNN
F 1 "4081" H 1650 2384 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 1650 2150 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 1650 2150 50  0001 C CNN
	3    1650 2150
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U13
U 4 1 5E38FA48
P 1650 2700
F 0 "U13" H 1650 3025 50  0000 C CNN
F 1 "4081" H 1650 2934 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 1650 2700 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 1650 2700 50  0001 C CNN
	4    1650 2700
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U14
U 1 1 5E39198B
P 3450 1050
F 0 "U14" H 3450 1375 50  0000 C CNN
F 1 "4081" H 3450 1284 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 3450 1050 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 3450 1050 50  0001 C CNN
	1    3450 1050
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U14
U 2 1 5E391991
P 3450 2150
F 0 "U14" H 3450 2475 50  0000 C CNN
F 1 "4081" H 3450 2384 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 3450 2150 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 3450 2150 50  0001 C CNN
	2    3450 2150
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U14
U 3 1 5E391997
P 3450 1600
F 0 "U14" H 3450 1925 50  0000 C CNN
F 1 "4081" H 3450 1834 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 3450 1600 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 3450 1600 50  0001 C CNN
	3    3450 1600
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U14
U 4 1 5E39199D
P 3450 2700
F 0 "U14" H 3450 3025 50  0000 C CNN
F 1 "4081" H 3450 2934 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 3450 2700 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 3450 2700 50  0001 C CNN
	4    3450 2700
	1    0    0    -1  
$EndComp
Text Label 1250 950  2    50   ~ 0
BIT1
Wire Wire Line
	1250 950  1350 950 
Text Label 1250 1500 2    50   ~ 0
BIT2
Wire Wire Line
	1350 1500 1250 1500
$Comp
L 4xxx:4050 U8
U 1 1 5E39DBFE
P 7050 3900
F 0 "U8" H 7050 4217 50  0000 C CNN
F 1 "4050" H 7050 4126 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 7050 3900 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 7050 3900 50  0001 C CNN
	1    7050 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 6850 4800 6850
Wire Wire Line
	4800 6850 4800 7100
Wire Wire Line
	4800 7100 4700 7100
Wire Wire Line
	4800 7100 4800 7350
Wire Wire Line
	4800 7350 4850 7350
Connection ~ 4800 7100
Wire Wire Line
	5450 6850 5550 6850
Wire Wire Line
	5450 7350 5500 7350
Text Label 5550 7350 0    50   ~ 0
CLOCK1
Text Label 5550 6850 0    50   ~ 0
CLOCK2
$Comp
L 4xxx:4050 U11
U 1 1 5E3A207D
P 5150 6850
F 0 "U11" H 5150 7167 50  0000 C CNN
F 1 "4050" H 5150 7076 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 5150 6850 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 5150 6850 50  0001 C CNN
	1    5150 6850
	1    0    0    -1  
$EndComp
Text Label 1250 1150 2    50   ~ 0
CLOCK1
Wire Wire Line
	1350 1150 1300 1150
Wire Wire Line
	1300 1150 1300 1700
Wire Wire Line
	1300 1700 1350 1700
Connection ~ 1300 1150
Wire Wire Line
	1300 1150 1250 1150
Wire Wire Line
	1300 1700 1300 2250
Wire Wire Line
	1300 2250 1350 2250
Connection ~ 1300 1700
Wire Wire Line
	1300 2250 1300 2800
Wire Wire Line
	1300 2800 1350 2800
Connection ~ 1300 2250
Text Label 3050 2050 2    50   ~ 0
BIT3
Wire Wire Line
	1350 2050 1250 2050
Text Label 1250 2600 2    50   ~ 0
BIT4
Wire Wire Line
	1350 2600 1250 2600
Text Label 3050 1150 2    50   ~ 0
CLOCK2
Wire Wire Line
	3150 1150 3100 1150
Wire Wire Line
	3100 1150 3100 1700
Wire Wire Line
	3100 1700 3150 1700
Connection ~ 3100 1150
Wire Wire Line
	3100 1150 3050 1150
Wire Wire Line
	3100 1700 3100 2250
Wire Wire Line
	3100 2250 3150 2250
Connection ~ 3100 1700
Wire Wire Line
	3100 2250 3100 2800
Wire Wire Line
	3100 2800 3150 2800
Connection ~ 3100 2250
Text Label 3050 950  2    50   ~ 0
BIT5
Wire Wire Line
	3150 950  3050 950 
Text Label 3050 1500 2    50   ~ 0
BIT6
Text Label 1250 2050 2    50   ~ 0
BIT7
Text Label 3050 2600 2    50   ~ 0
BIT8
Wire Wire Line
	3150 1500 3050 1500
Wire Wire Line
	3150 2050 3050 2050
Wire Wire Line
	3150 2600 3050 2600
Text Label 2000 1050 0    50   ~ 0
PULSE1
Wire Wire Line
	1950 1050 2000 1050
Text Label 2000 1600 0    50   ~ 0
PULSE2
Text Label 3800 2150 0    50   ~ 0
PULSE3
Text Label 2000 2700 0    50   ~ 0
PULSE4
Wire Wire Line
	1950 1600 2000 1600
Wire Wire Line
	1950 2150 2000 2150
Wire Wire Line
	1950 2700 2000 2700
Text Label 3800 1050 0    50   ~ 0
PULSE5
Text Label 3800 1600 0    50   ~ 0
PULSE6
Text Label 2000 2150 0    50   ~ 0
PULSE7
Text Label 3800 2700 0    50   ~ 0
PULSE8
Wire Wire Line
	3750 1050 3800 1050
Wire Wire Line
	3750 1600 3800 1600
Wire Wire Line
	3750 2150 3800 2150
Wire Wire Line
	3750 2700 3800 2700
$Comp
L 4xxx:4081 U?
U 5 1 5E3C12D9
P 3150 3900
AR Path="/5DE395EF/5E3C12D9" Ref="U?"  Part="5" 
AR Path="/5DE3964E/5DF7D519/5E3C12D9" Ref="U14"  Part="5" 
F 0 "U14" H 3380 3946 50  0000 L CNN
F 1 "4081" H 3380 3855 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 3150 3900 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 3150 3900 50  0001 C CNN
	5    3150 3900
	1    0    0    -1  
$EndComp
$Comp
L power:+12VA #PWR?
U 1 1 5E3C12E6
P 3450 3350
AR Path="/5DE395EF/5E3C12E6" Ref="#PWR?"  Part="1" 
AR Path="/5DE3964E/5DF7D519/5E3C12E6" Ref="#PWR0172"  Part="1" 
F 0 "#PWR0172" H 3450 3200 50  0001 C CNN
F 1 "+12VA" H 3465 3523 50  0000 C CNN
F 2 "" H 3450 3350 50  0001 C CNN
F 3 "" H 3450 3350 50  0001 C CNN
	1    3450 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3350 3150 3350
$Comp
L Device:C C?
U 1 1 5E3C12EE
P 3800 3950
AR Path="/5DE395EF/5E3C12EE" Ref="C?"  Part="1" 
AR Path="/5DE3964E/5DF7D519/5E3C12EE" Ref="C29"  Part="1" 
F 0 "C29" H 3915 3996 50  0000 L CNN
F 1 "100n" H 3915 3905 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D8.0mm_W2.5mm_P5.00mm" H 3838 3800 50  0001 C CNN
F 3 "~" H 3800 3950 50  0001 C CNN
	1    3800 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4450 3450 4450
Wire Wire Line
	3150 4450 3150 4400
Wire Wire Line
	3800 3350 3800 3800
Wire Wire Line
	3150 3350 3150 3400
$Comp
L 4xxx:4081 U?
U 5 1 5E3C40B1
P 2050 3900
AR Path="/5DE395EF/5E3C40B1" Ref="U?"  Part="5" 
AR Path="/5DE3964E/5DF7D519/5E3C40B1" Ref="U13"  Part="5" 
F 0 "U13" H 2280 3946 50  0000 L CNN
F 1 "4081" H 2280 3855 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 2050 3900 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 2050 3900 50  0001 C CNN
	5    2050 3900
	-1   0    0    -1  
$EndComp
$Comp
L power:+12VA #PWR?
U 1 1 5E3C40BE
P 1700 3350
AR Path="/5DE395EF/5E3C40BE" Ref="#PWR?"  Part="1" 
AR Path="/5DE3964E/5DF7D519/5E3C40BE" Ref="#PWR0173"  Part="1" 
F 0 "#PWR0173" H 1700 3200 50  0001 C CNN
F 1 "+12VA" H 1715 3523 50  0000 C CNN
F 2 "" H 1700 3350 50  0001 C CNN
F 3 "" H 1700 3350 50  0001 C CNN
	1    1700 3350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1700 3350 2050 3350
$Comp
L Device:C C?
U 1 1 5E3C40C6
P 1400 3950
AR Path="/5DE395EF/5E3C40C6" Ref="C?"  Part="1" 
AR Path="/5DE3964E/5DF7D519/5E3C40C6" Ref="C28"  Part="1" 
F 0 "C28" H 1515 3996 50  0000 L CNN
F 1 "100n" H 1515 3905 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D8.0mm_W2.5mm_P5.00mm" H 1438 3800 50  0001 C CNN
F 3 "~" H 1400 3950 50  0001 C CNN
	1    1400 3950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1400 3350 1400 3800
Wire Wire Line
	2050 3350 2050 3400
Wire Notes Line
	4750 550  4750 4850
Wire Notes Line
	4750 4850 550  4850
Wire Notes Line
	550  4850 550  550 
Wire Notes Line
	550  550  4750 550 
Text Notes 600  4800 0    50   ~ 0
pulses outputs
Wire Wire Line
	5650 2500 5650 2350
Connection ~ 5650 2350
Wire Wire Line
	5650 2350 5150 2350
Text Label 10100 1550 0    50   ~ 0
BIT3
Text Label 10100 1950 0    50   ~ 0
BIT5
Text Label 10100 2150 0    50   ~ 0
BIT6
Text Label 10100 2550 0    50   ~ 0
BIT8
Wire Wire Line
	10000 1550 10100 1550
Wire Wire Line
	10000 1950 10100 1950
Wire Wire Line
	10000 2150 10100 2150
Wire Wire Line
	10000 2550 10100 2550
Wire Wire Line
	7700 2400 7700 2450
Wire Wire Line
	7700 2450 8050 2450
Wire Wire Line
	2050 4400 2050 4450
Wire Wire Line
	1400 4450 1700 4450
$Comp
L 4xxx:4050 U8
U 2 1 5E416579
P 5700 3900
F 0 "U8" H 5700 4217 50  0000 C CNN
F 1 "4050" H 5700 4126 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 5700 3900 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 5700 3900 50  0001 C CNN
	2    5700 3900
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U8
U 3 1 5E41660D
P 5700 4400
F 0 "U8" H 5700 4717 50  0000 C CNN
F 1 "4050" H 5700 4626 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 5700 4400 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 5700 4400 50  0001 C CNN
	3    5700 4400
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U8
U 4 1 5E41668C
P 5150 5450
F 0 "U8" H 5150 5767 50  0000 C CNN
F 1 "4050" H 5150 5676 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 5150 5450 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 5150 5450 50  0001 C CNN
	4    5150 5450
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U8
U 5 1 5E416706
P 5150 6000
F 0 "U8" H 5150 6317 50  0000 C CNN
F 1 "4050" H 5150 6226 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 5150 6000 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 5150 6000 50  0001 C CNN
	5    5150 6000
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U11
U 6 1 5E416791
P 8400 4400
F 0 "U11" H 8400 4717 50  0000 C CNN
F 1 "4050" H 8400 4626 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 8400 4400 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 8400 4400 50  0001 C CNN
	6    8400 4400
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U11
U 7 1 5E41681F
P 2550 6300
F 0 "U11" H 2780 6346 50  0000 L CNN
F 1 "4050" H 2780 6255 50  0000 L CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 2550 6300 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 2550 6300 50  0001 C CNN
	7    2550 6300
	1    0    0    -1  
$EndComp
Text Label 8000 3900 2    50   ~ 0
BIT3
Text Label 6650 3900 2    50   ~ 0
BIT5
Text Label 6650 4400 2    50   ~ 0
BIT6
Text Label 9450 4400 2    50   ~ 0
BIT8
Text Label 5300 3900 2    50   ~ 0
BIT1
Text Label 5300 4400 2    50   ~ 0
BIT2
Text Label 9450 3900 2    50   ~ 0
BIT7
Text Label 8000 4400 2    50   ~ 0
BIT4
$Comp
L 4xxx:4050 U11
U 2 1 5E41C03D
P 5150 7350
F 0 "U11" H 5150 7667 50  0000 C CNN
F 1 "4050" H 5150 7576 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 5150 7350 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 5150 7350 50  0001 C CNN
	2    5150 7350
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U11
U 3 1 5E41C043
P 7050 4400
F 0 "U11" H 7050 4717 50  0000 C CNN
F 1 "4050" H 7050 4626 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 7050 4400 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 7050 4400 50  0001 C CNN
	3    7050 4400
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U11
U 4 1 5E41C049
P 9850 3900
F 0 "U11" H 9850 4217 50  0000 C CNN
F 1 "4050" H 9850 4126 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 9850 3900 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 9850 3900 50  0001 C CNN
	4    9850 3900
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U11
U 5 1 5E41C04F
P 9850 4400
F 0 "U11" H 9850 4717 50  0000 C CNN
F 1 "4050" H 9850 4626 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 9850 4400 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 9850 4400 50  0001 C CNN
	5    9850 4400
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U8
U 6 1 5E41C055
P 8400 3900
F 0 "U8" H 8400 4217 50  0000 C CNN
F 1 "4050" H 8400 4126 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 8400 3900 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 8400 3900 50  0001 C CNN
	6    8400 3900
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U8
U 7 1 5E42142B
P 1400 6300
F 0 "U8" H 1630 6346 50  0000 L CNN
F 1 "4050" H 1630 6255 50  0000 L CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 1400 6300 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 1400 6300 50  0001 C CNN
	7    1400 6300
	1    0    0    -1  
$EndComp
$Comp
L power:+12VA #PWR?
U 1 1 5E4218C1
P 2200 5300
AR Path="/5DE395EF/5E4218C1" Ref="#PWR?"  Part="1" 
AR Path="/5DE3964E/5DF7D519/5E4218C1" Ref="#PWR0174"  Part="1" 
F 0 "#PWR0174" H 2200 5150 50  0001 C CNN
F 1 "+12VA" H 2215 5473 50  0000 C CNN
F 2 "" H 2200 5300 50  0001 C CNN
F 3 "" H 2200 5300 50  0001 C CNN
	1    2200 5300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1400 5800 1400 5700
Wire Wire Line
	1400 5700 2000 5700
Wire Wire Line
	2550 5800 2550 5700
Wire Wire Line
	1400 6800 1400 6850
Wire Wire Line
	1400 6850 2000 6850
Wire Wire Line
	2550 6850 2550 6800
$Comp
L Device:C C?
U 1 1 5E428B0A
P 3150 6000
AR Path="/5DE395EF/5E428B0A" Ref="C?"  Part="1" 
AR Path="/5DE3964E/5DF7D519/5E428B0A" Ref="C26"  Part="1" 
F 0 "C26" H 3265 6046 50  0000 L CNN
F 1 "100n" H 3265 5955 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D8.0mm_W2.5mm_P5.00mm" H 3188 5850 50  0001 C CNN
F 3 "~" H 3150 6000 50  0001 C CNN
	1    3150 6000
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E428BA4
P 2000 6050
AR Path="/5DE395EF/5E428BA4" Ref="C?"  Part="1" 
AR Path="/5DE3964E/5DF7D519/5E428BA4" Ref="C25"  Part="1" 
F 0 "C25" H 2115 6096 50  0000 L CNN
F 1 "100n" H 2115 6005 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D8.0mm_W2.5mm_P5.00mm" H 2038 5900 50  0001 C CNN
F 3 "~" H 2000 6050 50  0001 C CNN
	1    2000 6050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2000 5700 2000 5900
Wire Wire Line
	2000 6200 2000 6850
Connection ~ 2000 6850
Wire Wire Line
	2000 6850 2550 6850
Wire Wire Line
	2550 5700 3150 5700
Wire Wire Line
	3150 5700 3150 5850
Wire Wire Line
	3150 6150 3150 6850
Wire Wire Line
	3150 6850 2550 6850
Connection ~ 2550 6850
Wire Wire Line
	5300 3900 5400 3900
Wire Wire Line
	5300 4400 5400 4400
Wire Wire Line
	8000 3900 8100 3900
Wire Wire Line
	8000 4400 8100 4400
Wire Wire Line
	9450 4400 9550 4400
Wire Wire Line
	9450 3900 9550 3900
Wire Wire Line
	6650 4400 6750 4400
Text Label 6100 3900 0    50   ~ 0
GATE1
Text Label 6100 4400 0    50   ~ 0
GATE2
Wire Wire Line
	6000 4400 6050 4400
Text Label 8800 3900 0    50   ~ 0
GATE3
Wire Wire Line
	8700 3900 8750 3900
Text Label 8800 4400 0    50   ~ 0
GATE4
Wire Wire Line
	8700 4400 8750 4400
Text Label 7450 3900 0    50   ~ 0
GATE5
Wire Wire Line
	7350 3900 7400 3900
Text Label 7450 4400 0    50   ~ 0
GATE6
Wire Wire Line
	7350 4400 7400 4400
Text Label 10250 3900 0    50   ~ 0
GATE7
Wire Wire Line
	10150 3900 10200 3900
Text Label 10250 4400 0    50   ~ 0
GATE8
Wire Wire Line
	10150 4400 10200 4400
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J26
U 1 1 5E4A2632
P 8150 5850
F 0 "J26" H 8200 6367 50  0000 C CNN
F 1 "GATES_EXP" H 8200 6276 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x08_P2.54mm_Vertical" H 8150 5850 50  0001 C CNN
F 3 "~" H 8150 5850 50  0001 C CNN
	1    8150 5850
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J27
U 1 1 5E4A26A0
P 9900 5850
F 0 "J27" H 9950 6367 50  0000 C CNN
F 1 "PULSES_EXP" H 9950 6276 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x08_P2.54mm_Vertical" H 9900 5850 50  0001 C CNN
F 3 "~" H 9900 5850 50  0001 C CNN
	1    9900 5850
	-1   0    0    1   
$EndComp
Wire Notes Line
	4850 550  4850 3250
Wire Notes Line
	4850 3250 8900 3250
Wire Notes Line
	8900 3250 8900 550 
Wire Notes Line
	8900 550  4850 550 
Text Notes 8000 3200 0    50   ~ 0
pulses expander logic
Wire Wire Line
	5250 1100 5250 2750
Wire Wire Line
	5150 2350 5150 2950
Text HLabel 3800 2500 2    50   Output ~ 0
PULSE_8_BOTTOM
Wire Wire Line
	3750 2700 3750 2500
Wire Wire Line
	3750 2500 3800 2500
Connection ~ 3750 2700
Text HLabel 2000 1950 2    50   Output ~ 0
PULSE_7_BOTTOM
Wire Wire Line
	3750 2150 3750 1950
Wire Wire Line
	3750 1950 3800 1950
Connection ~ 3750 2150
$Comp
L Regulator_Linear:L78L09_TO92 U9
U 1 1 5E623896
P 1000 5700
F 0 "U9" H 1000 5942 50  0000 C CNN
F 1 "L78L09_TO92" H 1000 5851 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 1000 5925 50  0001 C CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/15/55/e5/aa/23/5b/43/fd/CD00000446.pdf/files/CD00000446.pdf/jcr:content/translations/en.CD00000446.pdf" H 1000 5650 50  0001 C CNN
	1    1000 5700
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:L78L09_TO92 U12
U 1 1 5E6239FC
P 3550 5700
F 0 "U12" H 3550 5942 50  0000 C CNN
F 1 "L78L09_TO92" H 3550 5851 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 3550 5925 50  0001 C CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/15/55/e5/aa/23/5b/43/fd/CD00000446.pdf/files/CD00000446.pdf/jcr:content/translations/en.CD00000446.pdf" H 3550 5650 50  0001 C CNN
	1    3550 5700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3250 5700 3150 5700
Connection ~ 3150 5700
Wire Wire Line
	3850 5700 3900 5700
Wire Wire Line
	3900 5700 3900 5350
Wire Wire Line
	3900 5350 2200 5350
Wire Wire Line
	650  5350 650  5700
Wire Wire Line
	650  5700 700  5700
Wire Wire Line
	1400 5700 1300 5700
Connection ~ 1400 5700
Wire Wire Line
	2200 5300 2200 5350
Connection ~ 2200 5350
Wire Wire Line
	2200 5350 650  5350
Wire Wire Line
	3550 6000 3550 6850
Wire Wire Line
	3550 6850 3150 6850
Connection ~ 3150 6850
Wire Wire Line
	1000 6000 1000 6850
Wire Wire Line
	1000 6850 1400 6850
Connection ~ 1400 6850
Wire Notes Line
	4000 4950 550  4950
Wire Notes Line
	550  4950 550  7100
Wire Notes Line
	550  7100 4000 7100
Wire Notes Line
	4000 7100 4000 4950
Text Notes 3950 7050 2    50   ~ 0
buffers +9V supply. May be bypassed
Wire Notes Line
	6950 5000 11150 5000
Wire Notes Line
	11150 5000 11150 6450
Wire Notes Line
	11150 6450 6950 6450
Wire Notes Line
	6950 6450 6950 5000
Text Notes 7000 6400 0    50   ~ 0
expanders outputs
Text HLabel 3800 1400 2    50   Output ~ 0
PULSE_6_BOTTOM
Wire Wire Line
	3800 1400 3750 1400
Wire Wire Line
	3750 1400 3750 1600
Connection ~ 3750 1600
Text HLabel 3800 850  2    50   Output ~ 0
PULSE_5_BOTTOM
Wire Wire Line
	3800 850  3750 850 
Wire Wire Line
	3750 850  3750 1050
Connection ~ 3750 1050
Text HLabel 2000 2500 2    50   Output ~ 0
PULSE_4_BOTTOM
Text HLabel 3800 1950 2    50   Output ~ 0
PULSE_3_BOTTOM
Text HLabel 2000 1400 2    50   Output ~ 0
PULSE_2_BOTTOM
Text HLabel 2000 850  2    50   Output ~ 0
PULSE_1_BOTTOM
Wire Wire Line
	2000 2500 1950 2500
Wire Wire Line
	1950 2500 1950 2700
Connection ~ 1950 2700
Wire Wire Line
	2000 850  1950 850 
Wire Wire Line
	1950 850  1950 1050
Connection ~ 1950 1050
Wire Wire Line
	2000 1400 1950 1400
Wire Wire Line
	1950 1400 1950 1600
Connection ~ 1950 1600
Wire Wire Line
	2000 1950 1950 1950
Wire Wire Line
	1950 1950 1950 2150
Connection ~ 1950 2150
$Comp
L power:+12VA #PWR0177
U 1 1 5E373D50
P 8100 5250
F 0 "#PWR0177" H 8100 5100 50  0001 C CNN
F 1 "+12VA" H 8100 5400 50  0000 C CNN
F 2 "" H 8100 5250 50  0001 C CNN
F 3 "" H 8100 5250 50  0001 C CNN
	1    8100 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 5450 7850 5450
Wire Wire Line
	8400 5450 8350 5450
$Comp
L power:-12VA #PWR0178
U 1 1 5E38C670
P 8800 5600
F 0 "#PWR0178" H 8800 5450 50  0001 C CNN
F 1 "-12VA" H 8800 5750 50  0000 C CNN
F 2 "" H 8800 5600 50  0001 C CNN
F 3 "" H 8800 5600 50  0001 C CNN
	1    8800 5600
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0179
U 1 1 5E4068A9
P 1700 4450
F 0 "#PWR0179" H 1700 4200 50  0001 C CNN
F 1 "GND" H 1705 4277 50  0000 C CNN
F 2 "" H 1700 4450 50  0001 C CNN
F 3 "" H 1700 4450 50  0001 C CNN
	1    1700 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0180
U 1 1 5E406A1C
P 3450 4450
F 0 "#PWR0180" H 3450 4200 50  0001 C CNN
F 1 "GND" H 3455 4277 50  0000 C CNN
F 2 "" H 3450 4450 50  0001 C CNN
F 3 "" H 3450 4450 50  0001 C CNN
	1    3450 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0181
U 1 1 5E406D50
P 8050 2450
F 0 "#PWR0181" H 8050 2200 50  0001 C CNN
F 1 "GND" H 8055 2277 50  0000 C CNN
F 2 "" H 8050 2450 50  0001 C CNN
F 3 "" H 8050 2450 50  0001 C CNN
	1    8050 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0182
U 1 1 5E406FC6
P 2000 6850
F 0 "#PWR0182" H 2000 6600 50  0001 C CNN
F 1 "GND" H 2005 6677 50  0000 C CNN
F 2 "" H 2000 6850 50  0001 C CNN
F 3 "" H 2000 6850 50  0001 C CNN
	1    2000 6850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0183
U 1 1 5E408356
P 8700 5300
F 0 "#PWR0183" H 8700 5050 50  0001 C CNN
F 1 "GND" H 8700 5150 50  0000 C CNN
F 2 "" H 8700 5300 50  0001 C CNN
F 3 "" H 8700 5300 50  0001 C CNN
	1    8700 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 5450 8400 5250
Wire Wire Line
	8400 5250 8100 5250
Wire Wire Line
	7800 5450 7800 5250
Wire Wire Line
	7800 5250 8100 5250
Connection ~ 8100 5250
Wire Wire Line
	8500 5550 8500 5300
Wire Wire Line
	8500 5250 8700 5250
Wire Wire Line
	8700 5250 8700 5300
Wire Wire Line
	8350 5550 8500 5550
Wire Wire Line
	7850 5550 7700 5550
Wire Wire Line
	7700 5550 7700 5300
Wire Wire Line
	7700 5300 8500 5300
Connection ~ 8500 5300
Wire Wire Line
	8500 5300 8500 5250
Wire Wire Line
	8600 5650 8600 5550
Wire Wire Line
	8600 5550 8800 5550
Wire Wire Line
	8800 5550 8800 5600
Wire Wire Line
	8350 5650 8450 5650
Wire Wire Line
	8450 5650 8450 5350
Wire Wire Line
	8450 5350 7600 5350
Wire Wire Line
	7600 5350 7600 5650
Wire Wire Line
	7600 5650 7850 5650
Connection ~ 8450 5650
Wire Wire Line
	8450 5650 8600 5650
Text Label 9600 5750 2    50   ~ 0
CLOCK1
Text Label 10100 5750 0    50   ~ 0
CLOCK1
Text Label 8350 5750 0    50   ~ 0
CLOCK2
Text Label 7850 5750 2    50   ~ 0
CLOCK2
Text Label 8350 6150 0    50   ~ 0
GATE1
Text Label 7850 6150 2    50   ~ 0
GATE2
Text Label 8350 5950 0    50   ~ 0
GATE5
Text Label 7850 5950 2    50   ~ 0
GATE6
Text Label 8350 6050 0    50   ~ 0
GATE3
Text Label 7850 6050 2    50   ~ 0
GATE4
Text Label 8350 5850 0    50   ~ 0
GATE7
Text Label 7850 5850 2    50   ~ 0
GATE8
$Comp
L power:+12VA #PWR0184
U 1 1 5E4AACEA
P 9850 5250
F 0 "#PWR0184" H 9850 5100 50  0001 C CNN
F 1 "+12VA" H 9850 5400 50  0000 C CNN
F 2 "" H 9850 5250 50  0001 C CNN
F 3 "" H 9850 5250 50  0001 C CNN
	1    9850 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 5450 9600 5450
Wire Wire Line
	10150 5450 10100 5450
$Comp
L power:-12VA #PWR0185
U 1 1 5E4AACF2
P 10550 5600
F 0 "#PWR0185" H 10550 5450 50  0001 C CNN
F 1 "-12VA" H 10550 5750 50  0000 C CNN
F 2 "" H 10550 5600 50  0001 C CNN
F 3 "" H 10550 5600 50  0001 C CNN
	1    10550 5600
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0186
U 1 1 5E4AACF8
P 10450 5300
F 0 "#PWR0186" H 10450 5050 50  0001 C CNN
F 1 "GND" H 10450 5150 50  0000 C CNN
F 2 "" H 10450 5300 50  0001 C CNN
F 3 "" H 10450 5300 50  0001 C CNN
	1    10450 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 5450 10150 5250
Wire Wire Line
	10150 5250 9850 5250
Wire Wire Line
	9550 5450 9550 5250
Wire Wire Line
	9550 5250 9850 5250
Connection ~ 9850 5250
Wire Wire Line
	10250 5550 10250 5300
Wire Wire Line
	10250 5250 10450 5250
Wire Wire Line
	10450 5250 10450 5300
Wire Wire Line
	10100 5550 10250 5550
Wire Wire Line
	9600 5550 9450 5550
Wire Wire Line
	9450 5550 9450 5300
Wire Wire Line
	9450 5300 10250 5300
Connection ~ 10250 5300
Wire Wire Line
	10250 5300 10250 5250
Wire Wire Line
	10350 5650 10350 5550
Wire Wire Line
	10350 5550 10550 5550
Wire Wire Line
	10550 5550 10550 5600
Wire Wire Line
	10100 5650 10200 5650
Wire Wire Line
	10200 5650 10200 5350
Wire Wire Line
	10200 5350 9350 5350
Wire Wire Line
	9350 5350 9350 5650
Wire Wire Line
	9350 5650 9600 5650
Connection ~ 10200 5650
Wire Wire Line
	10200 5650 10350 5650
Text Label 9600 5850 2    50   ~ 0
PULSE8
Text Label 10100 5850 0    50   ~ 0
PULSE7
Text Label 9600 5950 2    50   ~ 0
PULSE6
Text Label 10100 5950 0    50   ~ 0
PULSE5
Text Label 10100 6150 0    50   ~ 0
PULSE1
Text Label 9600 6150 2    50   ~ 0
PULSE2
Text Label 10100 6050 0    50   ~ 0
PULSE3
Text Label 9600 6050 2    50   ~ 0
PULSE4
Connection ~ 1700 4450
Wire Wire Line
	1700 4450 2050 4450
Wire Wire Line
	1400 4100 1400 4450
Connection ~ 1700 3350
Wire Wire Line
	1700 3350 1400 3350
Connection ~ 3450 3350
Wire Wire Line
	3450 3350 3800 3350
Connection ~ 3450 4450
Wire Wire Line
	3450 4450 3800 4450
Wire Wire Line
	3800 4100 3800 4450
Connection ~ 8050 1350
Wire Wire Line
	8050 1350 8350 1350
Connection ~ 8050 2450
Wire Wire Line
	8050 2450 8350 2450
Wire Wire Line
	8350 2100 8350 2450
Text Label 10100 1150 0    50   ~ 0
BIT1
Text Label 10100 1350 0    50   ~ 0
BIT2
Wire Wire Line
	10100 1150 10000 1150
Wire Wire Line
	10000 1350 10100 1350
Text Label 10100 1750 0    50   ~ 0
BIT4
Text Label 10100 2350 0    50   ~ 0
BIT7
Wire Wire Line
	10000 1750 10100 1750
Wire Wire Line
	10000 2350 10100 2350
Text Label 5650 900  2    50   ~ 0
PULSE1
Text Label 5650 1250 2    50   ~ 0
PULSE2
Text Label 5650 2500 2    50   ~ 0
PULSE4
Text Label 5650 2150 2    50   ~ 0
PULSE7
Wire Wire Line
	5250 1100 5650 1100
Wire Wire Line
	5650 1100 5650 1250
Connection ~ 5650 1100
Wire Wire Line
	5650 1100 5950 1100
Text HLabel 10000 2750 0    50   Input ~ 0
STEP_12
Text HLabel 10000 2950 0    50   Input ~ 0
STEP_16
Text Label 10100 2750 0    50   ~ 0
STEP12
Text Label 10100 2950 0    50   ~ 0
STEP16
Wire Wire Line
	10000 2750 10100 2750
Wire Wire Line
	10000 2950 10100 2950
Text Label 4800 5450 2    50   ~ 0
STEP12
Text Label 4800 6000 2    50   ~ 0
STEP16
Wire Wire Line
	4800 5450 4850 5450
Wire Wire Line
	4800 6000 4850 6000
Text HLabel 5500 5450 2    50   Output ~ 0
STEP_12_BOTTOM
Text HLabel 5500 6000 2    50   Output ~ 0
STEP_16_BOTTOM
Wire Wire Line
	5450 5450 5500 5450
Wire Wire Line
	5450 6000 5500 6000
Text HLabel 6100 3700 2    50   Output ~ 0
GATE_1
Text HLabel 6100 4200 2    50   Output ~ 0
GATE_2
Wire Wire Line
	6100 4200 6050 4200
Wire Wire Line
	6050 4200 6050 4400
Connection ~ 6050 4400
Wire Wire Line
	6050 4400 6100 4400
Wire Wire Line
	6100 3900 6050 3900
Wire Wire Line
	6100 3700 6050 3700
Wire Wire Line
	6050 3700 6050 3900
Connection ~ 6050 3900
Wire Wire Line
	6050 3900 6000 3900
Text HLabel 7450 3700 2    50   Output ~ 0
GATE_5
Wire Wire Line
	7450 3700 7400 3700
Wire Wire Line
	7400 3700 7400 3900
Connection ~ 7400 3900
Wire Wire Line
	7400 3900 7450 3900
Text HLabel 7450 4200 2    50   Output ~ 0
GATE_6
Wire Wire Line
	7400 4200 7450 4200
Wire Wire Line
	7400 4200 7400 4400
Connection ~ 7400 4400
Wire Wire Line
	7400 4400 7450 4400
Text HLabel 8800 3700 2    50   Output ~ 0
GATE_3
Wire Wire Line
	8800 3700 8750 3700
Wire Wire Line
	8750 3700 8750 3900
Connection ~ 8750 3900
Wire Wire Line
	8750 3900 8800 3900
Text HLabel 8800 4200 2    50   Output ~ 0
GATE_4
Wire Wire Line
	8800 4200 8750 4200
Wire Wire Line
	8750 4200 8750 4400
Connection ~ 8750 4400
Wire Wire Line
	8750 4400 8800 4400
Text HLabel 10250 3700 2    50   Output ~ 0
GATE_7
Wire Wire Line
	10250 3700 10200 3700
Wire Wire Line
	10200 3700 10200 3900
Connection ~ 10200 3900
Wire Wire Line
	10200 3900 10250 3900
Wire Wire Line
	10200 4400 10200 4200
Wire Wire Line
	10200 4200 10250 4200
Connection ~ 10200 4400
Wire Wire Line
	10200 4400 10250 4400
Text HLabel 10250 4200 2    50   Output ~ 0
GATE_8
Wire Wire Line
	6650 3900 6750 3900
Wire Wire Line
	6600 1000 6600 1250
Wire Wire Line
	6600 1250 5750 1250
Wire Wire Line
	5750 1250 5750 1550
Wire Wire Line
	5750 1550 5950 1550
Connection ~ 6600 1000
Wire Wire Line
	6600 1000 6900 1000
Wire Wire Line
	6600 2250 6600 1850
Wire Wire Line
	6600 1850 5750 1850
Wire Wire Line
	5750 1850 5750 1750
Wire Wire Line
	5750 1750 5950 1750
Connection ~ 6600 2250
Wire Wire Line
	6600 2250 6900 2250
Wire Wire Line
	6550 1650 6700 1650
$Comp
L Connector_Generic:Conn_01x01 J19
U 1 1 5E8A32F4
P 6900 1650
F 0 "J19" H 6979 1692 50  0000 L CNN
F 1 "1247" H 6979 1601 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 6900 1650 50  0001 C CNN
F 3 "~" H 6900 1650 50  0001 C CNN
	1    6900 1650
	1    0    0    -1  
$EndComp
Text HLabel 6100 7100 2    50   Output ~ 0
CLOCK_BUFF
Text Notes 6000 6900 0    50   ~ 0
Buffered clock to drive\nshift registers
Wire Wire Line
	5500 7350 5500 7100
Wire Wire Line
	5500 7100 6100 7100
Connection ~ 5500 7350
Wire Wire Line
	5500 7350 5550 7350
$EndSCHEMATC
